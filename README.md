q8-rs
======
[![pipeline status](https://gitlab.com/scsn/q8-rs/badges/master/pipeline.svg)](https://gitlab.com/scsn/q8-rs/-/commits/master)

Library and tools to communicate with the Quanterra/Kinemetrics Q8
Qantix series seismic data acquisition systems, built in Rust. Currently only
supports the Station Monitor protocol over UDP; there are no plans at this time to
support the full data acquisition protocol over TCP.

Installation
------------

If you have [Rust and Cargo](https://www.rust-lang.org/tools/install) installed,
you can install q8-rs directly from this repository:

    cargo install --git https://gitlab.com/scsn/q8-rs.git

Otherwise visit the [Releases page](https://gitlab.com/scsn/q8-rs/-/releases)
to download pre-built binaries for Linux, Mac, and Windows.

Usage
-----

### q8ping

Ping a Q8 Station Monitor:

    q8ping 192.0.2.10

The default port number is 7331. Alternative port numbers can be specified with an
optional second argument. Hostnames and IPv6 addresses are also accepted:

    q8ping myq8.example.net 8331
    q8ping 2001:db8:0:ff::10

Request detailed status instead of basic information with `-s` (or `--status`):

    q8ping -s 192.0.2.10

Display output in JSON or YAML format for parsing with another tool. Raw values from
the Q8 are displayed in this mode, so you may need to do additional transformations
on the output depending on your needs (e.g. convert Q8 epoch times - seconds
since 1 Jan 2016 - into human-readable dates):

    q8ping -f json 192.0.2.10
    q8ping -s -f yaml 192.0.2.10

See help and full usage info:

    q8ping --help

### q8report

Display a summary report of the Q8s listed in the file stations.csv:

    q8report stations.csv

The CSV file must be in the following format, including the header line:

    station,address,port
    CI.TST01,192.0.2.10,7331
    CI.TST02,tst02.example.org,7331
    CI.TST03,2001:db8::30,7331

Note that hostnames/FQDNs, IPv4 addresses, and IPv6 addresses are accepted.
The output format is subject to change without notice until a v1.0 release.

Copyright and License
---------------------

Primary author and maintainer: Christopher Bruton <<cpbruton@caltech.edu>>

Copyright © 2021 California Institute of Technology

Licensed under the [MIT License](LICENSE).

Disclaimer
----------

This software is not developed by and is not affiliated with Kinemetrics, Inc. or
Quanterra, Inc. This software is not based on and is not a derivative work of lib660
(the GPL-licensed reference implementation of the protocol written in C).

This work is still in development. Use at your own risk.
