// Copyright (c) 2021 California Institute of Technology

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

use std::error::Error;
use bytes::{Bytes, BytesMut, Buf, BufMut};
use crate::qcrc::CRC;
use core::fmt::{Debug, Display};
use chrono::prelude::*;
use chrono::Duration;
use serde::{Serialize, Deserialize};

// Command type consts
type Cmd = u8;
pub const SM_REQ: Cmd = 28;
pub const SM_RESP: Cmd = 29;

// Response types
pub const SM_RESP_INFO: u8 = 0;
pub const SM_RESP_STATUS: u8 = 1;

// Current QDP version
pub const QDP_VERSION: u8 = 0;

// Quanterra epoch (January 1, 2016 UTC)
pub fn q8_epoch() -> DateTime<Utc> {
    Utc.ymd(2016, 1, 1).and_hms(0, 0, 0)
}

// Blockette Types
// Useful for pattern matching. Each implementation needs to return its type with blockette_type().
#[derive(Debug)]
pub enum Blockettes {
    QdpHeader(QdpHeader),
    SmReqHeader(SmReqHeader),
    SmRespHeader(SmRespHeader),
    SmRespInfo(SmRespInfo),
    SmRespStatus(SmRespStatus),
    SmRespStatusSM(SmRespStatusSM),
}

// Error types
#[derive(Debug)]
pub enum PacketError {
    UnsupportedVersion,
    TooShort,
    LengthCheckFailure,
    MissingData,
    ChecksumFailure,
    ParseError
}

#[derive(Debug)]
pub struct BlocketteParseError;

#[derive(Debug, Serialize, Deserialize)]
pub struct Packet {
    header: QdpHeader,
    pub blockettes: Vec<Box<dyn Blockette>>,
}

// impl Debug for dyn Blockette {
//     fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
//         write!(f, "{:?}", self.to_bytes())
//     }

// }

impl Packet {

    pub fn from_bytes(buf: &mut Bytes) -> Result<Packet, PacketError> {
        // Parse raw bytes from the UDP packet into a QDP packet

        let length = buf.len();

        // Minimum length is 12 (4 byte QDP header + 4 byte data + CRC)
        if length < 12 { return Err(PacketError::TooShort); }

        // Read the header
        let header = QdpHeader::from_bytes(&mut buf.slice(0..4)).unwrap();

        // Check the version and length
        if header.version != QDP_VERSION { return Err(PacketError::UnsupportedVersion); }
        if header.length != !header.length_check { return Err(PacketError::LengthCheckFailure); }

        // Check actual length against length in header (given in num_blocks)
        // 4B header + asserted data length + 4B crc
        let expected_length = 4 + 4*(header.length + 1) + 4;
        if length < expected_length.into() { return Err(PacketError::MissingData); }

        // Split off the CRC (and any padding etc.) from the end of the packet
        let expected_checksum = buf.split_off((expected_length - 4).into()).get_u32();

        // Calculate the actual checksum
        let checksum = CRC.checksum(&buf);

        // Compare checksums
        if expected_checksum != checksum { return Err(PacketError::ChecksumFailure); }

        // All good with data integrity at this point ✨


        // Now process the blockettes based on the command

        // Set up storage for our blockettes
        let mut blockettes: Vec<Box<dyn Blockette>> = Vec::new();

        // We originally read the header without advancing, so need to advance 4 bytes
        buf.advance(4);

        match header.command {
            SM_RESP => {
                // Get the response header (4 bytes)
                let resp_header = SmRespHeader::from_bytes(&mut buf.split_to(4)).unwrap();

                // Proceed based on response type (status or info)
                match resp_header.response_type {
                    SM_RESP_INFO => {
                        // Parse the response info; that's all that should be left in this packet
                        blockettes.push(Box::new(SmRespInfo::from_bytes(&mut buf.split_to(16)).unwrap()));
                    },
                    SM_RESP_STATUS => {
                        // Parse the base status blockette (always included)
                        let base_status = SmRespStatus::from_bytes(&mut buf.split_to(20)).unwrap();
                        // Make a copy of the status bitmap (u32)
                        let status_bitmap = base_status.status_bitmap;

                        // Push the base status
                        blockettes.push(Box::new(base_status));

                        // Push additional blockettes if applicable
                        if (status_bitmap & 0b00000001) != 0 {
                            blockettes.push(Box::new(SmRespStatusSM::from_bytes(&mut buf.split_to(88)).unwrap()));
                        }
                    },
                    _ => return Err(PacketError::ParseError),
                }
            },
            _ => todo!()
        }




        // Construct the final packet and return
        Ok(Packet {
            header: header,
            blockettes: blockettes
        })


        //todo!()

    }

    pub fn to_bytes(&self) -> Bytes {
        // Provide the full packet including CRC, ready to send inside UDP
        // This must only be called after the QdpHeader is fully built

        // Buffer to build
        let mut buf = BytesMut::with_capacity(1032);

        // Add the header
        buf.put(self.header.to_bytes());

        // Add each blockette
        for blockette in &self.blockettes {
            buf.put(blockette.to_bytes());
        }

        // Calculate the checksum and append
        let checksum = CRC.checksum(&buf);
        buf.put_u32(checksum);

        // Freeze and return
        buf.freeze()
    }

    fn new(cmd: Cmd, seq: u8, blockettes: Vec<Box<dyn Blockette>>) -> Packet {
        // This private function constructs the final packet except CRC, it does not check
        // that the cmd ID matches the blockettes supplied.

        // BytesMut object to hold the raw data
        let mut payload = BytesMut::with_capacity(1024);

        // Add each blockette to the payload
        for blockette in &blockettes {
            payload.put(blockette.to_bytes());
        }

        // Get the total length of the payload
        let length = payload.len();

        // Length should be a multiple of 4; if it's not, there's a bug somewhere
        assert_eq!(length % 4, 0);

        // The length value in the header is length/4 (number of u32 blocks)
        let num_blocks = (length / 4) as u8;

        // Build the header
        let qdp_header = QdpHeader {
            version: QDP_VERSION,
            command: cmd,
            sequences: seq,
            length: num_blocks,
            length_check: !num_blocks
        };

        // Return the completed packet
        Packet {
            header: qdp_header,
            blockettes: blockettes
        }
    }

    pub fn new_req_info() -> Packet {
        // Build a new basic info request packet

        let request = SmReqHeader::new(0, 0);
        let seq: u8 = 0;

        // Build and return the packet
        Packet::new(SM_REQ, seq, vec!(Box::new(request)))

    }

    pub fn new_req_status(status_bitmap: u16) -> Packet {
        let request = SmReqHeader::new(1, status_bitmap);
        let seq: u8 = 0;

        Packet::new(SM_REQ, seq, vec!(Box::new(request)))
    }

}

// Trait for blockettes (section of payload)
#[typetag::serde(tag = "type")]
pub trait Blockette: Debug {
    fn from_bytes(data: &mut Bytes) -> Result<Self, BlocketteParseError> where Self: Sized;
    fn to_bytes(&self) -> Bytes;
    fn blockette_type(&self) -> Blockettes;
    fn display(&self) {
        println!("{:#?}", self)
    }
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct QdpHeader {
    pub version: u8,
    pub command: u8,
    pub sequences: u8,
    pub length: u8,
    pub length_check: u8
}

#[typetag::serde]
impl Blockette for QdpHeader {
    fn blockette_type(&self) -> Blockettes { Blockettes::QdpHeader(*self) }
    fn to_bytes(&self) -> Bytes {
        let mut buf = BytesMut::with_capacity(4);
        buf.put_u8(self.version << 5 | self.command);
        buf.put_u8(self.sequences);
        buf.put_u8(self.length);
        buf.put_u8(!self.length); // length check
        buf.freeze()
    }

    fn from_bytes(buf: &mut Bytes) -> Result<QdpHeader, BlocketteParseError> {
        match buf.len() {
            4 => Ok(QdpHeader {
                version: buf[0] >> 5,
                command: buf.get_u8() & 0b00011111,
                sequences: buf.get_u8(),
                length: buf.get_u8(),
                length_check: buf.get_u8()
            }),
            _ => Err(BlocketteParseError)
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct SmReqHeader {
    request_type: u8,
    status_bitmap: u16
}

impl SmReqHeader {
    pub fn new(request_type: u8, status_bitmap: u16) -> SmReqHeader {
        SmReqHeader {
            request_type,
            status_bitmap
        }
    }
}

#[typetag::serde]
impl Blockette for SmReqHeader {
    fn blockette_type(&self) -> Blockettes { Blockettes::SmReqHeader(*self) }
    fn to_bytes(&self) -> Bytes {
        let mut buf = BytesMut::with_capacity(4);
        buf.put_u8(self.request_type);
        buf.put_u8(0); // spare
        buf.put_u16(self.status_bitmap);
        buf.freeze()
    }

    fn from_bytes(buf: &mut Bytes) -> Result<SmReqHeader, BlocketteParseError> {
        match buf.len() {
            4 => Ok(SmReqHeader {
                request_type: buf.get_u8(),
                status_bitmap: { buf.advance(1); buf.get_u16() }
            }),
            _ => Err(BlocketteParseError)
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct SmRespHeader {
    response_type: u8,
    blockette_size: u8
}

#[typetag::serde]
impl Blockette for SmRespHeader {
    fn blockette_type(&self) -> Blockettes { Blockettes::SmRespHeader(*self) }
    fn to_bytes(&self) -> Bytes {
        let mut buf = BytesMut::with_capacity(4);
        buf.put_u8(self.response_type);
        buf.put_u8(0); // spare
        buf.put_u8(0); // spare
        buf.put_u8(self.blockette_size);
        buf.freeze()
    }

    fn from_bytes(buf: &mut Bytes) -> Result<SmRespHeader, BlocketteParseError> {
        match buf.len() {
            4 => Ok(SmRespHeader {
                response_type: buf.get_u8(),
                blockette_size: { buf.advance(2); buf.get_u8() }
            }),
            _ => Err(BlocketteParseError)
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct SmRespInfo {
    pub serial_number: u64,
    pub property_tag: u32,
    pub backend_version: u16,
    pub frontend_version: u16
}

#[typetag::serde]
impl Blockette for SmRespInfo {
    fn blockette_type(&self) -> Blockettes { Blockettes::SmRespInfo(*self) }
    fn to_bytes(&self) -> Bytes {
        let mut buf = BytesMut::with_capacity(16);
        buf.put_u64(self.serial_number);
        buf.put_u32(self.property_tag);
        buf.put_u16(self.backend_version);
        buf.put_u16(self.frontend_version);
        buf.freeze()
    }

    fn from_bytes(buf: &mut Bytes) -> Result<SmRespInfo, BlocketteParseError> {
        match buf.len() {
            16 => Ok(SmRespInfo {
                serial_number: buf.get_u64(),
                property_tag: buf.get_u32(),
                backend_version: buf.get_u16(),
                frontend_version: buf.get_u16()
            }),
            _ => Err(BlocketteParseError)
        }
    }

    fn display(&self) {
        println!("  Serial number: {:016X}", self.serial_number);
        println!("  Property tag: {}", self.property_tag);
        println!("  Backend version: {}", ver_from(self.backend_version));
        println!("  Frontend version: {}", ver_from(self.frontend_version));
    }
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct SmRespStatus {
    last_frontend_reboot: u32,
    last_supervisor_reboot: u32,
    last_backend_reboot: u32,
    total_frontend_reboots: u32,
    status_bitmap: u32
}

#[typetag::serde]
impl Blockette for SmRespStatus {
    fn blockette_type(&self) -> Blockettes { Blockettes::SmRespStatus(*self) }
    fn to_bytes(&self) -> Bytes {
        let mut buf = BytesMut::with_capacity(20);
        buf.put_u32(self.last_frontend_reboot);
        buf.put_u32(self.last_supervisor_reboot);
        buf.put_u32(self.last_backend_reboot);
        buf.put_u32(self.total_frontend_reboots);
        buf.put_u32(self.status_bitmap);
        buf.freeze()
    }
    fn from_bytes(buf: &mut Bytes) -> Result<SmRespStatus, BlocketteParseError> {
        match buf.len() {
            20 => Ok(SmRespStatus {
                last_frontend_reboot: buf.get_u32(),
                last_supervisor_reboot: buf.get_u32(),
                last_backend_reboot: buf.get_u32(),
                total_frontend_reboots: buf.get_u32(),
                status_bitmap: buf.get_u32()
            }),
            _ => Err(BlocketteParseError)
        }
    }

    fn display(&self) {
        println!("Status Header:");
        println!("  Last frontend reboot: {}", date_from(self.last_frontend_reboot));
        println!("  Last system supervisor reboot: {}", date_from(self.last_supervisor_reboot));
        println!("  Last backend reboot: {}", date_from(self.last_backend_reboot));
        println!("  Total frontend reboots: {}", self.total_frontend_reboots);
    }

}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct SmRespStatusSM {
    status_type: u8,
    flags: u8,
    pll_status: u8,
    blockette_size: u8,
    gpin1: u8,
    gpin2: u8,
    packet_buffer: u8,
    humidity: u8,
    sensor_a_current: u16,
    sensor_b_current: u16,
    pub channel_1_boom: i16,
    pub channel_2_boom: i16,
    pub channel_3_boom: i16,
    pub channel_4_boom: i16,
    pub channel_5_boom: i16,
    pub channel_6_boom: i16,
    system_current: u16,
    antenna_current: u16,
    input_voltage: u16,
    temperature: i16,
    antenna_voltage: u16,
    // _spare0: u16,
    station_time: u32,
    offset_micros: i32,
    total_time_s: u32,
    power_on_time_s: u32,
    time_last_resync: u32,
    total_resyncs: u32,
    min_since_gps_lock: u16,
    sensor_a_gain_status: u8,
    sensor_b_gain_status: u8,
    sensor_control_bitmap: u16,
    fault_code: u8,
    // _spare1: u8,
    gps_power_status: u8,
    gps_fix_status: u8,
    pub clock_quality_percent: u8,
    calibrator_status: u8,
    elevation: f32,
    latitude: f32,
    longitude: f32
    // _spare2: u32
}

#[typetag::serde]
impl Blockette for SmRespStatusSM {
    fn blockette_type(&self) -> Blockettes { Blockettes::SmRespStatusSM(*self) }
    fn to_bytes(&self) -> Bytes {
        todo!()
    }

    fn from_bytes(buf: &mut Bytes) -> Result<SmRespStatusSM, BlocketteParseError> {
        match buf.len() {
            88 => Ok(SmRespStatusSM {
                status_type: buf.get_u8(),
                flags: buf.get_u8(),
                pll_status: buf.get_u8(),
                blockette_size: buf.get_u8(),
                gpin1: buf.get_u8(),
                gpin2: buf.get_u8(),
                packet_buffer: buf.get_u8(),
                humidity: buf.get_u8(),
                sensor_a_current: buf.get_u16(),
                sensor_b_current: buf.get_u16(),
                channel_1_boom: buf.get_i16(),
                channel_2_boom: buf.get_i16(),
                channel_3_boom: buf.get_i16(),
                channel_4_boom: buf.get_i16(),
                channel_5_boom: buf.get_i16(),
                channel_6_boom: buf.get_i16(),
                system_current: buf.get_u16(),
                antenna_current: buf.get_u16(),
                input_voltage: buf.get_u16(),
                temperature: buf.get_i16(),
                antenna_voltage: buf.get_u16(),
                // _spare0: buf.get_u16(),
                station_time: { buf.advance(2); buf.get_u32() },
                offset_micros: buf.get_i32(),
                total_time_s: buf.get_u32(),
                power_on_time_s: buf.get_u32(),
                time_last_resync: buf.get_u32(),
                total_resyncs: buf.get_u32(),
                min_since_gps_lock: buf.get_u16(),
                sensor_a_gain_status: buf.get_u8(),
                sensor_b_gain_status: buf.get_u8(),
                sensor_control_bitmap: buf.get_u16(),
                fault_code: buf.get_u8(),
                // _spare1: buf.get_u8(),
                gps_power_status: { buf.advance(1); buf.get_u8() },
                gps_fix_status: buf.get_u8(),
                clock_quality_percent: buf.get_u8(),
                calibrator_status: buf.get_u8(),
                elevation: buf.get_f32(),
                latitude: buf.get_f32(),
                longitude: buf.get_f32(),
                // _spare2: buf.get_u32()
            }),
            _ => Err(BlocketteParseError)

        }
    }

    fn display(&self) {
        println!("Station Monitor Status:");
        println!("  Flags: {:08b}", self.flags);
        println!("  PLL Status: {}", pll_string(self.pll_status));
        println!("  GPIN1 active time: {}%", self.gpin1);
        println!("  GPIN2 active time: {}%", self.gpin2);
        println!("  Packet buffer usage: {}%", self.packet_buffer);
        println!("  Humidity: {}%", self.humidity);
        println!("  Sensor A current: {} mA", self.sensor_a_current / 10);
        println!("  Sensor B current: {} mA", self.sensor_b_current / 10);
        println!("  Channel 1 boom: {:.3} V", self.channel_1_boom as f32 * 0.001);
        println!("  Channel 2 boom: {:.3} V", self.channel_2_boom as f32 * 0.001);
        println!("  Channel 3 boom: {:.3} V", self.channel_3_boom as f32 * 0.001);
        println!("  Channel 4 boom: {:.3} V", self.channel_4_boom as f32 * 0.001);
        println!("  Channel 5 boom: {:.3} V", self.channel_5_boom as f32 * 0.001);
        println!("  Channel 6 boom: {:.3} V", self.channel_6_boom as f32 * 0.001);
        println!("  System current: {} mA", self.system_current / 10);
        println!("  Antenna current: {} mA", self.antenna_current / 10);
        println!("  Input voltage: {:.3} V", self.input_voltage as f32 * 0.002);
        println!("  Temperature: {}°C", self.temperature as f32 / 10.0);
        println!("  Antenna voltage: {:.3} V", self.antenna_voltage as f32 * 0.002);
        println!("  Station time: {}", date_from(self.station_time));
        println!("  Microsecond offset: {} µs", self.offset_micros);
        println!("  Total time: {}", fmt_duration(Duration::seconds(self.total_time_s.into())));
        println!("  Power on time: {}", fmt_duration(Duration::seconds(self.power_on_time_s.into())));
        println!("  Last resync: {}", date_from(self.time_last_resync));
        println!("  Total resyncs: {}", self.total_resyncs);
        println!("  Minutes since GPS lock: {} min", self.min_since_gps_lock);
        println!("  Sensor A gain status: {}", self.sensor_a_gain_status);
        println!("  Sensor B gain status: {}", self.sensor_b_gain_status);
        println!("  Sensor A control bitmap: {:05b}", self.sensor_control_bitmap & 0xFF);
        println!("  Sensor B control bitmap: {:05b}", self.sensor_control_bitmap >> 8);
        println!("  Fault code: {}:{}", self.fault_code >> 4, self.fault_code & 0x0F);
        println!("  GPS power status: {}", gps_power_string(self.gps_power_status));
        println!("  GPS fix status: {}", gps_fix_string(self.gps_fix_status));
        println!("  Clock quality: {}%", self.clock_quality_percent);
        println!("  Calibrator status: {:03b}", self.calibrator_status);
        println!("  Elevation: {} m", self.elevation);
        println!("  Coordinates: {}°, {}°", self.latitude, self.longitude);
    }
}

// Helper function to convert dates from the Q8 epoch format
pub fn date_from(epoch: u32) -> DateTime<Utc> {
    q8_epoch() + Duration::seconds(epoch.into())
}

// Helper function to convert packed version number into a readable string
pub fn ver_from(ver: u16) -> String {
    format!("{}.{}", ver >> 8, ver & 0xFF)
}

// Format a duration into days, hours, minutes, seconds

pub fn fmt_duration(dur: Duration) -> String {
    let days = dur.num_days();
    let total_hours = dur.num_hours();
    let total_mins = dur.num_minutes();
    let total_secs = dur.num_seconds();

    let hours = total_hours - 24*days;
    let mins = total_mins - 60*total_hours;
    let secs = total_secs - 60*total_mins;

    format!("{} days {:02}:{:02}:{:02}", days, hours, mins, secs)
}

pub fn pll_string(pll_status: u8) -> String {
    match pll_status {
        0 => "Not enabled",
        1 => "Hold",
        2 => "Tracking",
        3 => "Locked",
        _ => "Unknown"
    }.to_string()
}

pub fn gps_power_string(gps_power_status: u8) -> String {
    match gps_power_status {
        0 => "Off",
        1 => "Powered off (PLL lock)",
        2 => "Powered off (time limit)",
        3 => "Powered off (by command)",
        4 => "Powered off (fault; antenna current high)",
        5 => "Powered on (automatically)",
        6 => "Powered on (by command)",
        7 => "Cold start",
        _ => "Unknown"
    }.to_string()
}

pub fn gps_fix_string(gps_fix_status: u8) -> String {
    match gps_fix_status {
        0 => "Off (unknown fix)",
        1 => "Off (no fix)",
        2 => "Off (last fix 1D)",
        3 => "Off (last fix 2D)",
        4 => "Off (last fix 3D)",
        5 => "On (no fix)",
        6 => "On (unknown fix)",
        7 => "On (1D fix)",
        8 => "On (2D fix)",
        9 => "On (3D fix)",
        _ => "Unknown"
    }.to_string()
}
