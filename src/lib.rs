// Copyright (c) 2021 California Institute of Technology

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

pub mod qdp;

// CRC checksum features
pub mod qcrc {
    use crc::{Crc, Algorithm};
    // Set up CRC function
    pub const CRC: Crc<u32> = Crc::<u32>::new(&Q_CRC_ALG);

    // Set up Quanterra CRC algorithm
    const Q_CRC_ALG: Algorithm<u32> = Algorithm {
        poly: 0x56070368,
        init: 0x00000000,
        refin: false,
        refout: false,
        xorout: 0x00000000,
        check: 0x37C7CA30,
    residue: 0x00000000 // TBD?
    };

    // Test CRC function with standard check
    #[test]
    fn test_crc_check() {
        assert_eq!(Q_CRC_ALG.check, CRC.checksum(b"123456789"));
    }

    // Test CRC function with real QDP data
    #[test]
    fn test_crc_qdp() {
        let check = 0x0ca17e08;
        let data: [u8; 20] = [0x38, 0x02, 0x00, 0x0c,
        0x00, 0x03, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x04,
        0x54, 0x31, 0x14, 0xc0,
        0x39, 0x65, 0xc3, 0x45];
        assert_eq!(check, CRC.checksum(&data));
    }
}
