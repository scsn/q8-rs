// Copyright (c) 2021 California Institute of Technology

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

use std::error::Error;
use std::net::{UdpSocket, SocketAddr, ToSocketAddrs};
use std::process;
use std::time::{Duration, Instant};

use bytes::{Bytes, BytesMut, Buf, BufMut};
use clap::{App, Arg, ArgMatches, crate_version, AppSettings};

use q8::qcrc::CRC;
use q8::qdp::{Packet, SmRespInfo};


fn main() {
    let matches = App::new("q8ping")
    .setting(AppSettings::ColoredHelp)
    .setting(AppSettings::UnifiedHelpMessage)
    .version(crate_version!())
    .author("Christopher Bruton <cpbruton@caltech.edu>")
    .about("Ping a Q8 Station Monitor with QDP SM_REQ packet.")
    .after_help("Report issues or contribute at https://gitlab.com/scsn/q8-rs")
    .arg(Arg::with_name("status")
       .short("s")
       .long("status")
       .help("Request logger status instead of logger info"))
    .arg(Arg::with_name("timeout")
       .short("t")
       .long("timeout")
       .default_value("5")
       .help("Seconds to wait for a reply from the Q8"))
    .arg(Arg::with_name("format")
        .short("f")
        .long("format")
        .possible_value("text")
        .possible_value("json")
        .possible_value("yaml")
        .default_value("text")
        .help("Output format"))
    .arg(Arg::with_name("address")
       .help("IP address or hostname")
       .required(true))
    .arg(Arg::with_name("port")
       .help("UDP port number")
       .default_value("7331"))
    .get_matches();

    if let Err(e) = run(matches) {
        println!("Application error: {}", e);
        process::exit(1);
    }
}

fn run(matches: ArgMatches) -> Result<(), Box<dyn Error>> {
    // Basic validation of arguments
    let address = matches.value_of("address").unwrap();
    let port: u16 = matches.value_of("port").unwrap().parse()
        .expect("Port must be an integer in range 0-65535.");
    let timeout: f32 = matches.value_of("timeout").unwrap().parse()
        .expect("Timeout must be a real number.");

    let format = matches.value_of("format").unwrap();

    // Validate timeout and reassign as duration
    assert!(timeout > 0.0, "Timeout must be a positive non-zero number.");
    let timeout = Duration::from_secs_f32(timeout);

    // Resolve the address
    let addr = (address, port).to_socket_addrs()?.next().unwrap();

    if format == "text" {
        println!("Trying to ping a Q8 at {:?}...", addr);
    }

    // Bind socket depending on address kind (IPv4 or IPv6)
    let socket = match addr {
        SocketAddr::V4(_) => UdpSocket::bind("0.0.0.0:0"),
        SocketAddr::V6(_) => UdpSocket::bind("[::]:0")
    }?;

    // "Connect" the socket to the address
    socket.connect(addr)?;

    // Build a new request info packet
    let packet = match matches.is_present("status") {
        false => Packet::new_req_info(),
        true => Packet::new_req_status(1)
    };

    // Debug: display the bytes created
    //dbg!(packet.to_bytes());

    // Send the bytes out the socket
    socket.send(&packet.to_bytes())?;

    // Mark the start time
    let start_time = Instant::now();

    // Set the timeout to listen
    socket.set_read_timeout(Some(timeout))?;

    // New buffer for the reply
    // Todo: use a better buffer object if possible e.g. BytesMut
    let mut reply = [0; 1500];

    // Wait for and process the reply
    match socket.recv(&mut reply) {
        Ok(amt) => {
            if format == "text" {
                println!("Received {} bytes in {} ms:", amt, start_time.elapsed().as_millis());
            }
            let mut bytes = Bytes::copy_from_slice(&reply[..amt]);

            // Parse the bytes into a Packet
            let reply_packet = Packet::from_bytes(&mut bytes).unwrap();

            match matches.value_of("format").unwrap() {
                "text" => display_reply(&reply_packet),
                "json" => println!("{}", serde_json::to_string_pretty(&reply_packet).unwrap()),
                "yaml" => println!("{}", serde_yaml::to_string(&reply_packet).unwrap()),
                _ => unreachable!()
            };
            Ok(())
        },
        Err(e) => {
            if format == "text" {
                println!("Error after {} ms: {:?}", start_time.elapsed().as_millis(), e);
            }
            Err(Box::new(e))
        }
    }

    // Ok(())
}

fn display_reply(packet: &Packet) {
    //dbg!(packet);
    for blockette in &packet.blockettes {
        blockette.display();
    }
}
