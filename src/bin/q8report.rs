// Copyright (c) 2021 California Institute of Technology

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

use std::collections::BTreeMap;
use std::error::Error;
use std::fs::File;
use std::net::{UdpSocket, SocketAddr, ToSocketAddrs};
use std::process;
use std::time::{Duration, Instant};

use bytes::{Bytes, BytesMut, Buf, BufMut};
use clap::{App, Arg, ArgMatches, crate_version, AppSettings};

use q8::qcrc::CRC;
use q8::qdp::{Packet, SmRespInfo, SmRespStatus, SmRespStatusSM, Blockettes, ver_from};

// For reading the station CSV file
// THe order is: station, (address, port number)
type StationRecord = (String, (String, u16));

fn main() {

    // Set up the argument parser
    let matches = App::new("q8report")
    .setting(AppSettings::ColoredHelp)
    .setting(AppSettings::UnifiedHelpMessage)
    .version(crate_version!())
    .author("Christopher Bruton <cpbruton@caltech.edu>")
    .about("Generate a summary status report of multiple Q8s.")
    .after_help("Report issues or contribute at https://gitlab.com/scsn/q8-rs")
    .arg(Arg::with_name("timeout")
        .short("t")
        .long("timeout")
        .default_value("5")
        .help("Seconds to wait for a reply from each Q8"))
    .arg(Arg::with_name("csvfile")
        .help("Filename of CSV-formatted list of stations")
        .required(true))
    .get_matches();

    if let Err(e) = run(matches) {
        println!("Application error: {}", e);
        process::exit(1);
    }
}

fn run(matches: ArgMatches) -> Result<(), Box<dyn Error>> {
    // Basic validation
    let timeout: f32 = matches.value_of("timeout").unwrap().parse()
        .expect("Timeout must be a real number.");

    // Validate timeout and reassign as duration
    assert!(timeout > 0.0, "Timeout must be a positive non-zero number.");
    let timeout = Duration::from_secs_f32(timeout);

    // Open the CSV file with the station list
    // Example of format (file should include header line):
    //
    // station,address,port
    // CI.NEE2,166.248.129.95,7331
    // CI.DSN01,dsn01,7331
    // CI.JVA,10.192.85.10,7331
    //
    // The address can be an IPv4 address, IPv6 address, hostname, or FQDN.
    //
    let mut rdr = csv::Reader::from_path(matches.value_of("csvfile").unwrap())?;

    // Parse each record and insert into a BTreeMap of names to socket addresses
    let mut station_addrs = BTreeMap::new();
    for result in rdr.deserialize() {
        let record: StationRecord = result?;
        station_addrs.insert(record.0, record.1.to_socket_addrs()?.next().unwrap());
    }

    // Print the header line
    println!("Station  Address               Status  RTT(ms) Versions       Clock  Boom Positions(V)");

    // Iterate over the hashmap
    for (station, saddr) in &station_addrs {

        // Bind a new socket depending on address kind (IPv4 or IPv6)
        let socket = match saddr {
            SocketAddr::V4(_) => UdpSocket::bind("0.0.0.0:0"),
            SocketAddr::V6(_) => UdpSocket::bind("[::]:0")
        }?;

        // "Connect" the socket to the station's socket address
        socket.connect(saddr)?;

        // Set the read timeout
        socket.set_read_timeout(Some(timeout))?;

        // Build an info request packet
        let packet = Packet::new_req_info();

        // Send the packet to the remote address
        socket.send(&packet.to_bytes())?;

        // Mark the start time
        let start_time = Instant::now();

        // Create a new buffer for the reply
        let mut reply = [0; 1500];

        // Print station name and address
        print!("{:8} {:21} ", station, saddr);

        // Wait for and process the reply
        match socket.recv(&mut reply) {
            Ok(amt) => {
                //println!("Received {} bytes in {} ms:", amt, start_time.elapsed().as_millis());
                let elapsed = start_time.elapsed().as_millis();
                
                // Copy the reply into a Bytes
                let mut bytes = Bytes::copy_from_slice(&reply[..amt]);

                // Parse the bytes into a Packet
                let reply_packet = Packet::from_bytes(&mut bytes).unwrap();

                // This is an info reply so there should only be one blockette
                let blockette = &reply_packet.blockettes[0];

                // Make sure it's a an info response
                match blockette.blockette_type() {
                    Blockettes::SmRespInfo(info) => {
                        //      Status  RTT(ms) Versions     Clock
                        print!("OK      {:<5}   {:<6} {:<6}  ", elapsed, ver_from(info.backend_version), ver_from(info.frontend_version));
                        //println!("{}", blockette.serial_number)

                        // Prepare and send a status request (use the same socket)
                        let packet = Packet::new_req_status(1);
                        socket.send(&packet.to_bytes())?;

                        // Receive the reply
                        let mut reply = [0; 1500];
                        match socket.recv(&mut reply) {
                            Ok(amt) => {
                                // Parse the reply bytes
                                let mut bytes = Bytes::copy_from_slice(&reply[..amt]);
                                let reply_packet = Packet::from_bytes(&mut bytes).unwrap();

                                // This time there should be multiple blockettes; we want the SmRespStatusSM blockette
                                for blockette in &reply_packet.blockettes {
                                    match blockette.blockette_type() {
                                        Blockettes::SmRespStatusSM(status) => {
                                            print!("{:<3}%   {:.2} {:.2} {:.2}\n",
                                                status.clock_quality_percent,
                                                status.channel_1_boom as f32 * 0.001,
                                                status.channel_2_boom as f32 * 0.001,
                                                status.channel_3_boom as f32 * 0.001)
                                        },
                                        _ => continue,
                                    }
                                }
                                
                            },
                            Err(_) => {
                                print!("error getting detailed status\n");
                            }
                        }

                    },
                    _ => todo!(),
                }
            },
            Err(e) => {
                print!("ERROR   {}\n", e);
            }
        }
    }    

    Ok(())
}
