Changelog
=========

Planned
-------

- Add additional status blockette types
- Convert JSON and YAML output values into sensible formats
- Improve display of some values in text format

v0.4.0
------------

- Add q8report, a new summary report of multiple Q8s.
- Colorize help message.
- Fix: Antenna voltage displayed incorrect value.
- Fix: Negative boom positions were not displayed correctly.

v0.1.0
------

Initial release.

